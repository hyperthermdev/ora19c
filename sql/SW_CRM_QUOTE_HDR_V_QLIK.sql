DROP VIEW APPS.SW_CRM_QUOTE_HDR_V;

/* Formatted on 10/22/2021 8:41:05 AM (QP5 v5.365) */
CREATE OR REPLACE FORCE VIEW APPS.SW_CRM_QUOTE_HDR_V
(
    QUOTEID,
    REFQUOTEID,
    STATUS,
    QUOTETYPE,
    REQUESTTYPE,
    OCLACCTNO,
    OCLSITENO,
    SLXACCTID,
    CONTACTNAME,
    CONTACTEMAIL,
    SLXONBEHALFID,
    ENTRYLANGUAGE,
    OUTPUTLANGUAGE,
    CURRENCY,
    EXCHRATE,
    LISTPRICE,
    NETPRICE,
    DISCPRICE,
    FINALIZEDATE,
    EXPIRATIONDATE,
    QUOTECREATEUSER,
    QUOTECREATEDATE,
    QUOTEMODIFYUSER,
    QUOTEMODIFYDATE,
    UUID,
    SUBSCRIPTIONENDDATE,
    PRODUCTFAMILY,
    QUOTEPRODUCTS
)
BEQUEATH DEFINER
AS
    SELECT quote_header_id
               quoteID,
           ref_quote_id
               RefQuoteID,
           INITCAP (quote_status)
               status,
           CASE
               WHEN UPPER (quote_type) = 'NORMAL' THEN ''
               WHEN UPPER (quote_type) = 'COURTESY' THEN 'Courtesy'
               WHEN UPPER (quote_type) = 'CHANNEL PARTNER' THEN 'CP'
               ELSE quote_type
           END
               QuoteType,
           INITCAP (quote_request_type)
               RequestType,
           ocl_account_number
               OCLAcctNo,
           ocl_site_number
               OCLSiteNo,
           slx_account_id
               SLXAcctID,
           slx_Contact_Name
               ContactName,
           slx_contact_email
               ContactEMail,
           slx_onbehalf_Account_ID
               SLXOnBehalfID,
           entry_language
               EntryLanguage,
           output_language
               Outputlanguage,
           currency,
           NVL (to_usd_cnvr_rate, 1)
               ExchRate,
           NVL (list_price_total, 0)
               ListPrice,
           NVL (unit_price_total, 0)
               NetPrice,
           NVL (quote_price_total, 0)
               DiscPrice,
           finalized_date
               FinalizeDate,
           Expiration_date
               ExpirationDate,
           Created_By
               QuoteCreateUser,
           Creation_Date
               QuoteCreateDate,
           Last_Updated_By
               QuoteModifyUser,
           Last_Updated_Date
               QuoteModifyDate,
           UUID,
           (SELECT MIN (ql.subscription_expiration_date)    subscription_end_date
              FROM apps.quote_lines ql
             WHERE     ql.subscription_expiration_date IS NOT NULL
                   AND ql.quote_header_id = qh.quote_header_id)
               subscriptionenddate,
           (SELECT MIN (pf.family)
              FROM apps.quote_lines  ql
                   INNER JOIN sw_product_families pf
                       ON ql.MB_INVENTORY_ITEM_ID = pf.MB_INVENTORY_ITEM_ID
             WHERE ql.quote_header_id = qh.quote_header_id)
               ProductFamily,
           (  SELECT LISTAGG (ql.BasePrdDesc, ', ')
                         WITHIN GROUP (ORDER BY ql.BasePrdDesc)    ProductList
                FROM (SELECT DISTINCT
                             QUOTE_HEADER_ID,
                             NVL (MSITL_mb.LONG_DESCRIPTION,
                                  MSITL_mb.DESCRIPTION)    BasePrdDesc
                        FROM apps.quote_lines
                             LEFT JOIN APPS.MTL_SYSTEM_ITEMS_B msib_mb
                                 ON     mb_INVENTORY_ITEM_ID =
                                        MSIB_mb.INVENTORY_ITEM_ID
                                    AND MSIB_mb.ORGANIZATION_ID = 103
                             LEFT JOIN APPS.MTL_SYSTEM_ITEMS_TL msitl_mb
                                 ON     msitl_mb.INVENTORY_ITEM_ID =
                                        MSIB_mb.INVENTORY_ITEM_ID
                                    AND MSITL_mb.LANGUAGE = 'US'
                                    AND MSITL_mb.ORGANIZATION_ID =
                                        MSIB_mb.ORGANIZATION_ID) ql
               WHERE 1 = 1 AND ql.QUOTE_HEADER_ID = qh.quote_header_id
            GROUP BY ql.quote_header_id)
               QuoteProducts
      FROM apps.quote_header qh
     WHERE     1 = 1
           AND SLX_ACCOUNT_ID IS NOT NULL
           AND ((qh.QUOTE_STATUS != 'Cancelled' and qh.CREATION_DATE >= '01-NOV-2018')                        --Rev5 --rev6
            OR (qh.QUOTE_STATUS = 'Cancelled' and qh.CREATION_DATE >= '01-NOV-2019'))                           --rev6

--Rev1: 2010-05-05 SHG add quoteType, refQuoteID, initcap status and requestType
--Rev2: 2010-07-16 SHG add exchRate field; change field names to remove US
--Rev3: 2010-07-16 SHG fix net / disc prices (they were swapped);
--Rev4: 2018.06.14 SHG add uuid, subscription_end_date (roll-up), Product Family (roll-up), QuoteProducts (comma delim list)
--Rev5: 2021-10-22 SHG Add limit to include only 2015 and newer quotes in sync
--Rev6: 2021-11-03 SHG Limits changed to only sync active quotes >= 2019.01.01 and cancelled/expired >= 2020.01.01
--Rev7: 2021-11-06 SHG change limits to use create_date 3 months earlier
;

GRANT READ ON APPS.SW_CRM_QUOTE_HDR_V TO XX_APPSRO;

GRANT READ ON APPS.SW_CRM_QUOTE_HDR_V TO XX_APPSRO_ALL;
