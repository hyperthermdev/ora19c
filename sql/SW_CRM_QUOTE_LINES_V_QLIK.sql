DROP VIEW APPS.SW_CRM_QUOTE_LINES_V;

/* Formatted on 10/22/2021 9:08:13 AM (QP5 v5.365) */
CREATE OR REPLACE FORCE VIEW APPS.SW_CRM_QUOTE_LINES_V
(
    QUOTELINEID,
    QUOTEHDRID,
    ACCOUNTID,
    LINETYPE,
    BASEPRDDESC,
    OCPRDDESC,
    ITEMDESC,
    ITEMNO,
    QUANTITY,
    LISTUNITPRICE,
    NETUNITPRICE,
    DISCUNITPRICE,
    DISCTYPE,
    DISCAMT,
    SUBSCRIPTIONENDDATE,
    SWID,
    QUOTEMODIFYUSER,
    QUOTEMODIFYDATE,
    QUOTECREATEUSER,
    QUOTECREATEDATE
)
BEQUEATH DEFINER
AS
    SELECT QL.QUOTE_LINE_ID
               QuoteLineID,
           QL.QUOTE_HEADER_ID
               QuoteHdrID,
           QH.SLX_ACCOUNT_ID
               AccountID,
           QL.QUOTE_LINE_TYPE
               LineType,
           NVL (MSITL_mb.LONG_DESCRIPTION, MSITL_mb.DESCRIPTION)
               BasePrdDesc,
           NVL (MSITL_oc.LONG_DESCRIPTION, MSITL_oc.DESCRIPTION)
               OCPrdDesc,
           NVL (MSITL.LONG_DESCRIPTION, MSITL.DESCRIPTION)
               ItemDesc,
           MSIB.segment1
               ItemNo,
           QL.QUANTITY,
           NVL (QL.LIST_PRICE, 0)
               ListUnitPrice,
           NVL (QL.UNIT_PRICE, 0)
               NetUnitPrice,
           NVL (QL.QUOTE_UNIT_PRICE, 0)
               DiscUnitPrice,
           QL.LINE_DISCOUNT_TYPE
               DiscType,
           NVL (QL.LINE_DISCOUNT_AMOUNT, 0)
               DiscAmt,
           QL.SUBSCRIPTION_EXPIRATION_DATE
               SubscriptionEndDate,
           QL.QUOTE_SWID_ID
               SWID,
           QL.LAST_UPDATED_BY
               QuoteModifyUser,
           QL.LAST_UPDATED_DATE
               QuoteModifyDate,
           QL.CREATED_BY
               QuoteCreateUser,
           QL.CREATION_DATE
               QuoteCreateDate
      FROM apps.quote_lines  ql
           INNER JOIN apps.quote_header qh
               ON QL.QUOTE_HEADER_ID = QH.QUOTE_HEADER_ID
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_B msib
               ON     ql.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
                  AND MSIB.ORGANIZATION_ID = 103
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_TL msitl
               ON     msitl.INVENTORY_ITEM_ID = MSIB.INVENTORY_ITEM_ID
                  AND MSITL.LANGUAGE = 'US'
                  AND MSITL.ORGANIZATION_ID = MSIB.ORGANIZATION_ID
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_B msib_mb
               ON     ql.mb_INVENTORY_ITEM_ID = MSIB_mb.INVENTORY_ITEM_ID
                  AND MSIB_mb.ORGANIZATION_ID = 103
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_TL msitl_mb
               ON     msitl_mb.INVENTORY_ITEM_ID = MSIB_mb.INVENTORY_ITEM_ID
                  AND MSITL_mb.LANGUAGE = 'US'
                  AND MSITL_mb.ORGANIZATION_ID = MSIB_mb.ORGANIZATION_ID
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_B msib_oc
               ON     ql.oc_INVENTORY_ITEM_ID = MSIB_oc.INVENTORY_ITEM_ID
                  AND MSIB_oc.ORGANIZATION_ID = 103
           LEFT JOIN APPS.MTL_SYSTEM_ITEMS_TL msitl_oc
               ON     msitl_oc.INVENTORY_ITEM_ID = MSIB_oc.INVENTORY_ITEM_ID
                  AND MSITL_oc.LANGUAGE = 'US'
                  AND MSITL_oc.ORGANIZATION_ID = MSIB_oc.ORGANIZATION_ID
     WHERE 1 = 1 AND qh.SLX_ACCOUNT_ID IS NOT NULL --Rev4
                 AND ((qh.QUOTE_STATUS != 'Cancelled' and qh.CREATION_DATE >= '01-NOV-2018')                        --Rev5
                  OR (qh.QUOTE_STATUS = 'Cancelled' and qh.CREATION_DATE >= '01-NOV-2019'))                         --Rev5

--Rev0: 2010-05-05 SHG New
--Rev1: 2010-05-19 SHG Add nvl long descriptions
--Rev2: 2010-07-23 SHG Add nvl on prices
--Rev3: 2010-08-22 SHG Add itemNo;
--Rev4: 2021-10-22 SHG Add limit to include only 2015 and newer quotes in sync
--Rev5: 2021-11-03 SHG Limits changed to only sync active quotes >= 2019.01.01 and cancelled/expired >= 2020.01.01
--Rev6: 2021-11-06 SHG change limits to use create_date 3 months earlier
;


GRANT READ ON APPS.SW_CRM_QUOTE_LINES_V TO XX_APPSRO;

GRANT READ ON APPS.SW_CRM_QUOTE_LINES_V TO XX_APPSRO_ALL;
