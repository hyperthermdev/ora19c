CREATE OR REPLACE FORCE VIEW APPS.SW_CRM_LICENSE_V ("SWID", "ACCOUNTID", "BASEPRDID", "BASEPRDNO", "BASEPRDDESC", "MBITEMID", "SUBSCRIPTIONENDDATE", "ISSUEDATE", "SOFTWARETYPE", "KEYTYPE", "NETWORKSEATS", "PROTECTIONTYPE", "PROTECTIONSCHEME", "HASPKEYID", "HASPPRODUCTKEY", "HASPACTIVATIONCODE", "CRYPSITECODE", "CRYPSITEKEY", "HDSN", "HDSTATUS", "OCL_ACCOUNT_NUMBER", "OCL_SITE_NUMBER", "SWIBCREATEDATE", "SWIBCREATEUSER", "SWIBMODIFYDATE", "SWIBMODIFYUSER", "SWPLEVEL", "EFFECTIVE_START_DATE", "EFFECTIVE_END_DATE") 
AS
    SELECT SWIB.SOFTWARE_ID
               SWID,
           SWIB.SLX_ACCOUNT_ID
               AccountID,
           base_v.BasePrdID,
           base_v.BasePrdNo,
           NVL (base_v.BasePrdDesc, SWIB.CONV_ITEM_DESCRIPTION)
               BasePrdDesc,
           swib.mb_inventory_item_id
               MBItemID,
           subsc_v.subscription_end_date
               SubScriptionEndDate,
           SWKD.CREATION_DATE
               IssueDate,
           INITCAP (SWIB.SOFTWARE_TYPE)
               SoftwareType,
           INITCAP (SWIB.KEY_TYPE)
               KeyType,
           seat_v.seats
               NetworkSeats,
           SWIB.PROTECTION_TYPE
               ProtectionType,
           SWIB.PROTECTION_SCHEME
               ProtectionScheme,
           SWKD.HASP_KEY_IDENTIFIER
               HaspKeyID,
           SWKD.HASP_PRODUCT_KEY
               HaspProductKey,
           SWKD.HASP_ACTIVATION_CODE
               HaspActivationCode,
           SWKD.CRYP_SITE_CODE
               CrypSiteCode,
           SWKD.CRYP_SITE_KEY
               CrypSiteKey,
           SWKD.CRYP_HARDWARE_ID
               HDSN,
           SWKD.CRYP_HARDWARE_STATUS
               HDStatus,
           SWIB.OCL_ACCOUNT_NUMBER,
           SWIB.OCL_SITE_NUMBER,
           SWIB.CREATION_DATE
               SWIBCreateDate,
           SWIB.CREATED_BY
               SWIBCreateUser,
           CASE
               WHEN SWIB.LAST_UPDATED_DATE <
                    NVL (SWKD.LAST_UPDATED_DATE, SWIB.LAST_UPDATED_DATE)
               THEN
                   CASE
                       WHEN SWKD.LAST_UPDATED_DATE <
                            max_component_modify_date
                       THEN
                           max_component_modify_date
                       ELSE
                           SWKD.LAST_UPDATED_DATE
                   END
               ELSE
                   CASE
                       WHEN SWIB.LAST_UPDATED_DATE <
                            max_component_modify_date
                       THEN
                           max_component_modify_date
                       ELSE
                           SWIB.LAST_UPDATED_DATE
                   END
           END
               SWIBModifyDate,
           SWIB.LAST_UPDATED_BY
               SWIBModifyUser,
           SWKD.SWP_LEVEL
               SWPLevel,
           swib.effective_start_date,
           swib.effective_end_date
      FROM APPS.SW_INSTALL_BASE  swib
           LEFT JOIN APPS.sw_product_families pf
               ON pf.MB_INVENTORY_ITEM_ID = swib.MB_INVENTORY_ITEM_ID  --Rev13
           LEFT JOIN APPS.SW_KEY_DETAIL swkd
               ON     SWKD.SOFTWARE_ID = SWIB.SOFTWARE_ID
                  AND SYSDATE BETWEEN SWKD.EFFECTIVE_START_DATE
                                  AND SWKD.EFFECTIVE_END_DATE
           LEFT JOIN
           (  SELECT SWC.SOFTWARE_ID, SUM (SWC.COMPONENT_QTY) seats
                FROM APPS.SW_COMPONENTS swc
               WHERE     UPPER (SWC.COMPONENT_TYPE) IN ('BASE')
                     AND SYSDATE BETWEEN SWC.EFFECTIVE_START_DATE
                                     AND SWC.EFFECTIVE_END_DATE
            GROUP BY SWC.SOFTWARE_ID) seat_v
               ON seat_v.software_id = swib.software_id        --roll up seats
           LEFT JOIN
           (  SELECT SWC.SOFTWARE_ID,
                     MIN (SWC.END_DATE_ACTIVE)     subscription_end_date
                FROM APPS.SW_COMPONENTS swc
               WHERE     UPPER (SWC.COMPONENT_TYPE) = 'SUBSCRIPTION'
                     AND SYSDATE BETWEEN SWC.EFFECTIVE_START_DATE
                                     AND SWC.EFFECTIVE_END_DATE
            GROUP BY SWC.SOFTWARE_ID) subsc_v
               ON subsc_v.software_id = swib.software_id --roll up subscriptions
           LEFT JOIN
           (  SELECT SWC.SOFTWARE_ID,
                     MAX (SWC.LAST_UPDATED_DATE)     max_component_modify_date
                FROM APPS.SW_COMPONENTS swc
               WHERE     UPPER (SWC.COMPONENT_TYPE) = 'SUBSCRIPTION'
                     AND SYSDATE BETWEEN SWC.EFFECTIVE_START_DATE
                                     AND SWC.EFFECTIVE_END_DATE
            GROUP BY SWC.SOFTWARE_ID) comp_md_v
               ON comp_md_v.software_id = swib.software_id --roll up comp modifydates
           LEFT JOIN
           (SELECT Software_ID,
                   BasePrdID,
                   BasePrdDesc,
                   BasePrdNo
              FROM (  SELECT SWC.SOFTWARE_ID,
                             MAX (msib.INVENTORY_ITEM_ID)     baseprdID,
                             MAX (MSITL.LONG_DESCRIPTION)     BasePrdDesc,
                             MAX (MSIB.SEGMENT1)              BasePrdNo
                        FROM APPS.SW_COMPONENTS swc
                             INNER JOIN APPS.MTL_SYSTEM_ITEMS_B msib
                                 ON     MSIB.INVENTORY_ITEM_ID =
                                        SWC.INVENTORY_ITEM_ID
                                    AND MSIB.ORGANIZATION_ID = 103
                             INNER JOIN APPS.MTL_SYSTEM_ITEMS_TL msitl
                                 ON     msitl.INVENTORY_ITEM_ID =
                                        msib.INVENTORY_ITEM_ID
                                    AND MSITL.LANGUAGE = 'US'
                                    AND MSITL.ORGANIZATION_ID =
                                        MSIB.ORGANIZATION_ID
                       WHERE     UPPER (SWC.COMPONENT_TYPE) = 'BASE'
                             --AND SYSDATE BETWEEN SWC.EFFECTIVE_START_DATE --Rev15
                             --                AND SWC.EFFECTIVE_END_DATE --Rev15
                             AND SWC.SOFTWARE_ID = software_id
                    GROUP BY SWC.SOFTWARE_ID)) base_v
               ON base_v.software_id = swib.software_id      -- base component
     WHERE     1 = 1
           AND NVL (pf.NO_SYNC, 'F') = 'F'                             --Rev13
           AND base_v.BasePrdID is not null                            --Rev14
           AND swib.protection_type != 'Crypkey'                       --Rev13
           AND SWIB.SLX_ACCOUNT_ID IS NOT NULL
           AND SWIB.effective_start_date <> SWIB.effective_end_date;
